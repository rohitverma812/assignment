<?php
declare(strict_types=1);
class DataService {
    private $url;

    public function __construct($url) {
        $this->url = $url;
    }

    public function getData() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        try{
            $response = curl_exec($ch);
        }catch(Exception $e){
            throw new Exception("Error occurred: " . $e->getMessage());
        }
        if ($response === false) {
            $error = curl_error($ch);
            curl_close($ch);
            throw new Exception("Error occurred");
        }
        curl_close($ch);
        $data = json_decode($response, false, 10000);
        if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception("Error decoding JSON: " . json_last_error_msg());
        }

        return $data;
    }
}

class Company {
    private $_dataService;
    const COMPANY_URL = "https://stage.shipglobal.in/internaltest/s1/companies.php";

    public function __construct() {
        $this->_dataService = new DataService(self::COMPANY_URL);
    }

    public function getCompanyData() {
        return $this->_dataService->getData();
    }
}

class Travel {
    private $_dataService;
    const TRAVEL_URL = "https://stage.shipglobal.in/internaltest/s1/travels.php";

    public function __construct() {
        $this->_dataService = new DataService(self::TRAVEL_URL);
    }

    public function getTravelData() {
        return $this->_dataService->getData();
    }
}

class TestScript {
    protected $_company, $_travel;

    public function __construct(
        Company $company = new Company(), 
        Travel $travel = new Travel()
        ) {
        $this->_company = $company;
        $this->_travel = $travel;
    }

    public function execute() {
        $companyData = $this->getCompanyData();
        $this->mergeTravelData($companyData);
        $dataTree =  $this->buildData($companyData);
        $this->updateCost($dataTree);
        return $dataTree;
    }

    function buildData($companies) {
        $items = json_decode(json_encode($companies), true);
        $itemsById = [];
        $dtt = [];
        foreach ($items as $item) {
            $dtt['id'] = $item['id'];
            $dtt['name'] = $item['name'];
            $dtt['cost'] = $item['individualCost'];
            $dtt['individualCost'] = $item['individualCost'];
            $dtt['children'] = [];
            $itemsById[$item['id']] = $dtt;
        }
        $rootItems = [];
        foreach ($items as $item) {
            if (isset($itemsById[$item['parentId']])) {
                $itemsById[$item['parentId']]['children'][] = [$item['id'] => &$itemsById[$item['id']]];
            } else {
                $rootItems[$item['id']] = &$itemsById[$item['id']];
            }
        }
        return $rootItems;
    }

    private function updateCost(&$tree) {
        $totalCost = 0;
        foreach ($tree as &$item) {
            $item['cost'] = $item['individualCost'];
            if (isset($item['children'])) {
                foreach ($item['children'] as &$child) {
                    $item['cost'] += $this->updateCost($child);
                }
            }
            $totalCost += $item['cost'];
        }
        return $totalCost;
    }
    
    private function getCompanyData() {
        return $this->_company->getCompanyData();
    }

    private function getTravelData() {
        return $this->_travel->getTravelData();
    }

    private function mergeTravelData(&$companies) {
        $companyPrices = [];
        foreach ($this->getTravelData() as $travel) {
            $companyId = $travel->companyId;
            $price = floatval($travel->price);
            if (!isset($companyPrices[$companyId])) {
                $companyPrices[$companyId] = $price;
            } else {
                $companyPrices[$companyId] += $price;
            }
        }
        foreach ($companies as &$company) {
            $companyId = $company->id;
            $company->individualCost = isset($companyPrices[$companyId]) ? $companyPrices[$companyId] : 0;
        }
    }
}

header('Content-Type: application/json'); // for pretty ^^
$testScript = new TestScript();
$result = $testScript->execute();
echo json_encode($result, JSON_PRETTY_PRINT); exit;
